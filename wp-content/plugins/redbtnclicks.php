<?php
/*
* Plugin Name: Red button clicks
*/

function rbclicks_init() {
    add_meta_box('rbclicks', 'Клики по красной кнопке',
        'rbclicks_showup', 'post', 'normal', 'default');
}

function rbclicks_showup($post, $box) {
    $data_list = get_post_meta($post->ID,'redbutton_data',false);
    echo '<table border="1"><tr><td>Время</td><td>IP</td></tr>';
    foreach($data_list as $data){
        echo '<tr>';
        echo '<td>'.$data['date'].'</td>';
        echo '<td>'.$data['ip'].'</td>';
        echo '<tr>';
    }
    echo '</table>';
}

add_action('add_meta_boxes', 'rbclicks_init');
?>