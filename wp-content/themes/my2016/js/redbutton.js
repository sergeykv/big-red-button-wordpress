jQuery(document).ready(function($) {

    $("#big-red-button").click(function () {

        var $self = $(this),
            data = {
                action: 'redbutton_action',
                id: $self.data('id')
            };

        // 'ajaxurl' не определена во фронте, поэтому мы добавили её аналог с помощью wp_localize_script()
        $.ajax({
            url: myajax.url,
            dataType: 'json',
            data: data,
            success: function(response) {
                if(response.error){
                    $("<div>"+response.error+"</div>").insertAfter($self);
                }else{
                    $self.addClass("done");
                    $self.val("OK");
                    $self.off( "click");
                    $("<div>IP: "+response.ip+", Время: "+response.date+"</div>").insertAfter($self);
                }
            }
        });

        return false;
    });

    return false;
});