<?php
// Подключаем стили
function my2016_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my2016_enqueue_styles' );

// Подключаем js
function my2016_enqueue_scripts() {

    wp_register_script( 'redbutton', get_stylesheet_directory_uri() . '/js/redbutton.js', array('jquery'));

    if( is_single() )
    {
        wp_enqueue_script('redbutton');
    }
}
add_action('wp_enqueue_scripts', 'my2016_enqueue_scripts');

// 'ajaxurl' не определена во фронте, поэтому добавим её аналог с помощью wp_localize_script()
function myajax_data(){

    wp_localize_script('redbutton', 'myajax',
        array(
            'url' => admin_url('admin-ajax.php')
        )
    );
}
add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );

// обработка ajax
function redbutton_callback() {

    $error="";

    $id = intval( $_GET['id'] );
    if(!$id){
        $error = "Ошибка! Не удалось определить id записи";
    }

    $ip = get_client_ip();
    if($ip == 'UNKNOWN'){
        $error = "Ошибка! Не удалось определить ip";
    }

    $date = current_time('mysql'); // = date('d.m.Y H:i:s', time());

    add_post_meta($id,'redbutton_data',array("ip" => $ip,"date" => $date));

    $res = array(
        "ip" => $ip,
        "date" => $date,
        "error" => $error
    );

    echo json_encode($res);

    wp_die();
}
add_action('wp_ajax_redbutton_action', 'redbutton_callback');
add_action('wp_ajax_nopriv_redbutton_action', 'redbutton_callback');


// ---------------------------------------------------------
// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

?>